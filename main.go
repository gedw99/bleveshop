package main

import (

	"fmt"
    
    // "github.com/blevesearch/bleve"
    
)



func main() {    
    
    // console app
    fmt.Println("BleveShop program running..")
    
    // Config
    
    
    /* 
    
    Everything is hardcoded for now, just to get it running.
    
    High Level Pipeline
    
    ETL (Extract, Transform, Load) Process
    - Extract 
        - CSV to JSON
        - Direct mapping Name for Name, with nothging else. This is just so we can get the CSV into a JSON format so its easier to work on it.
    
    - Tranform
        - Maybe we need to do a few changes to it so it works with Bleve ?
        
    - Load
        -  Setup Bleve Mappings
        - Pump the JSON in.
    
    Expose as Restful JSON API
    - Setup Router, that makes calls into Bleve
    
    Test
    - Write a basic harness to test at the Resful HTTP API level
    
    */
    
    
    
}
