#!/bin/bash

# Build everything with Recornisable filenames

GOOS="$(go env GOOS)"
GOARCH="$(go env GOARCH)"
ROOT_DIR=$PWD
ROOT_BIN_DIR=$ROOT_DIR"/_bin"

echo "Your GOOS is: "           $GOOS
echo "Your GOARCH is: "         $GOARCH
echo "Your PATH is: "           $ROOT_DIR

PROJ_NAME="bleveshop"
PROJ_NAMESPACE="bitbucket.org/gedw99/"$PROJ_NAME


# Web Server (for mockng)
APP_FOLDER="cmd/webserver"
APP_NAME=$PROJ_NAME"-webserver"
echo "# Building: " $APP_NAME
cd $ROOT_DIR"/"$APP_FOLDER

go build -o=$APP_NAME
cp $APP_NAME $ROOT_BIN_DIR
rm $APP_NAME



# set permissions
cd $ROOT_BIN_DIR/
chmod +x *







