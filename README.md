# bleveshop

This is an attempt to build an open source Web Site and Server that does Free Text Search and Faceted Classification Search.

## Status

Not yet. I will update when its ready.



## Technology choices


*Bleve* ( https://github.com/blevesearch/bleve ) is impresive, because Faceted Classification is pretty tough stuff to do well.
There are other options like ElasticSearch, Lucerne, Solr and , however Bleve is the only one that is 100% Go based.

*BoltDB* ( https://github.com/boltdb/bolt ) is the Database. This is 100% golang based, and so gives lots of deployment options.
It has its limitations in terms of scale, but Bleve team are already developing other databases to work with Bleve using a common API.



## What type of search functionality ?

The following functionality is provided, and can be combined together.

1. Category based search
 - A user is presented with a Product Category Tree.
 - As the user chooses a Node in the Tree:
    - the Tree expands with sub selections.
    - the Facetted Search GUI changes to that scope, and presents the Faceted Search Properties GUI.
    - the result items are returned, with highlights of the terms.
 - Next the user is free to expand the scope (using Categories) or tighten the scope (using Facets)

2. Faceted Classification based search
 - A user is presented with all the properties of the Content that are common (E.G TV Brand, TV Screen Size, Release Date).
 - As you choose different properties (E:G. TV Brand = "Sony"):
    - the other search Properties are filtered. 
    - the result items are returned, with highlights of the terms.

3. Free Text based Search
 - A user is presented with a Free Text Search box.
 - When the user inputs their search Term(s) the system:
    - the result items are returned, with highlights of the terms.


## Supported OS's ?
Because its go, basically anywhere:
- Windows, Linux, Mac, FreeBSD, Solaris.
- Raspberry PI 2 (ARM)
- IOS (ARM)
- Android (ARM)


## GUI 

The intent is to provide a clean and extensible RestFul JSON API, so that any GUI can be used.

What is included is:
- A Basic Web GUI Test Harness allowing testing the system and playing with it.
- A Test Fixture and Test Data for CI and stability.


## GUI Test Harness:
1. Select Category where you drill down OR Select from the Tag Cloud (NOTE. Tag Cloud is V2)

2. As you choose Category / Tag, show the Faceted Search Drop list, with the possible selections.

3. As you Select Facets, show the products.

4. When Showing the products, highlight where the facts match matchs in the Product Text.
Product Data will be paginated, but not using pagination buttons, but instead by pre calling for data as he User scrolls.


### Example Data
The  data to be imported is an array of JSON docs. 
The data is highly denormalised.
The data is in German and some other languages to ensure Bleve can handle it.

This basic data structure allows the various search patterns to be demonstrated:
Product:
- UUID
- Product Category (e.g "Cat01 > Cat01-01 > Cat 01-01-01")
 - This is recursive 
 - A Product instance can only exist in ONE Category
- Product Tags
 - A Product instance can exist in MANY Tags.
- Product Facts of:
 - Gender
 - Size
 - Brand
 - Price
 - PriceDiscount
 - DateCreated
- Images
 - This is a collection of images
 - We can store the images in Bolt, but will see how that pans out.
 



 
## RoadMap

- Languages
    - More of them in the Test Data to ensure stability

- OS
    - Windows, OSX, Linux (X86)
    - RaspberyPi (ARM)
    - Android (ARM)
    - IOS (ARM)
    
 - Refactor API and Model to make more general.