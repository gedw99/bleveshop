package models

// ProductCategory ...
type ProductCategory struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Product ...
type Product struct {
	EAN                  string `json:"ean"`
	ProductCategoryRefID ProductCategory
	ProductID            string `json:"productid"`
	Name                 string `json:"name"`
	TitleShort           string `json:"titleshort"`
	Description          string `json:"description"`
	Available            bool   `json:"available"` // For Deniis to use
	Gender               string `json:"gender"`

	Color    string `json:"color"`
	Material string `json:"material"`
	Brand    string `json:"brand"`

	Price        string `json:"price"`
	PriceReduced string `json:"pricereduced"`

	ImageOtherArray []string `json:"imagepatharray"`

}
