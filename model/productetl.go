package models

// Product ...
type Product struct {
	EAN              string `json:"ean"`
	ProductID        string `json:"productid"`
	StyleID          string `json:"styleid"`
	Produktname      string `json:"produktname"`
	ProductShortCopy string `json:"productshortcopy"`
	ProductCategory  string `json:"productcategory"`
	Geschlecht       string `json:"geschlecht"`
	Marke            string `json:"marke"`
	Farbe            string `json:"farbe"`
	Größe            string `json:"größe"`
    LongCopy       string `json:"longcopy"`
    BulletPoints        string `json:"bulletpoints"`
    BildURL            string `json:"bildurl"`
    ZusätzlicherBildlink2   string `json:"zusätzlicherbildlink2"`
    ZusätzlicherBildlink3   string `json:"zusätzlicherbildlink3"`
    ZusätzlicherBildlink4   string `json:"zusätzlicherbildlink4"`
    ZusätzlicherBildlink5   string `json:"zusätzlicherbildlink5"`
    ZusätzlicherBildlink6   string `json:"zusätzlicherbildlink6"`
    ProduktURL   string `json:"produkturl"`
}
