package controller

import (
	"fmt"
)


// Run ...
func Run() {    
    
    // console app
    fmt.Println("ETL Processor Started..")
    
    extract()
    
    transform()
    
    load()
    
    
    fmt.Println("ETL Processor Ended..")
}


func extract() {    
    
    // console app
    fmt.Println("Extract running..")
    
}

func transform() {    
    
    // console app
    fmt.Println("transform running..")
    
}

// load ...
func load() {    
    
    // console app
    fmt.Println("load running..")
    
    // push into Bleve
    
}



